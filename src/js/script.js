document.addEventListener("DOMContentLoaded", function(event) {

    //Un commentaire
    console.log("Ça fonctionne!!!");

    let connexion = new MovieDb();

    connexion.testMethode();

    //======================================================================================================================

    function reduireTexte (element, nbrMaxMot, sufix) {

        // Nous vérifions si les arguments "element" et "maxChar"
        // sont bien passé dans l'appel de la fonction.
        if (!element || !nbrMaxMot) return;

        // Nous récupèrons le texte de l'élément
        var texte = element.textContent;

        // Nous convertisons la chaine de caractères en tableau avec .split(' ').
        // Le split utilise un espace, ce qui va donner un tableau
        // avec chaque mot dans une cellule.
        texte = texte.split(' ');

        // Nous arrêtons la fonction s'il y a moins de mots que nbrMaxMot
        if(texte.length < nbrMaxMot) return;

        // Nous coupons le tableau avec .slice(0, maxChar)
        // pour récupérer seulement le nombre de mots voulu.
        texte = texte.slice(0, nbrMaxMot);

        // Nous recréons une chaine de caractère avec .join(' ').
        // Le join utilise un espace pour séparer chaque mot.
        // S'il y a un sufix, nous ajoutons le sufix à la fin du texte.
        texte = texte.join(' ') + (sufix ? sufix : '');

        // Nous réinjectons le texte dans l'élément.
        element.textContent = texte;

    };

// Nous sélectionnons tous les éléments avec la classe '.reduire'.
    var elements = document.querySelectorAll('.reduire');

// Pour chaque élément sélectionné, nous appelons la fonction.
    for (var indexElement in elements) {
        reduireTexte(elements[indexElement], 12, '...');
    }


});

//======================================================================================================================

class MovieDb {

    constructor(){
        this.APIKey = "44359036dc2a4cc786c575106d95844c";
        this.lang = "fr-CA";
        this.baseUrl = "https://api.themoviedb.org/3/";
        this.imgPath = "http://image.tmdb.org/t/p/";

        this.largeurAffiche = ["w92", "w154", "w185", "w342", "w500", "w780"];
        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 6;
        this.totalActeur = 6;

        console.log("constructeur")

    }


    testMethode(){

        var data = "{}";

        var xhr = new XMLHttpRequest();
        //xhr.withCredentials = true; // à enleverrrwrweeweerwrwerqqqwerqwrqerqqrewewewwerwqqwerqwrqqqqrqrqwewe23342342334234242423224234232342342343234234232342234

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === this.DONE) {
                //console.log(this.responseText);

                var donneesFilms = JSON.parse(this.responseText).results;

                console.log(donneesFilms[6].id);


            }
        });

        xhr.open("GET", this.baseUrl + "movie/top_rated?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send(data);


    }

}